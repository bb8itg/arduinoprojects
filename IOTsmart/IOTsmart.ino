#include <Arduino_MKRIoTCarrier.h>
MKRIoTCarrier carrier;
#include "buttonControl.h"
#include "displayControl.h"
#include "menuControll.h"
#include "menuOption.h"
#include "temperature.h"
#include "humidity.h"
#include "keyboardModule.h"
#include "displayMessage.h"


ButtonControl buttonControl(&carrier.Buttons);
DisplayControl displayControl(&carrier.display);

char testMessage[256];
char testMessagePrint[] = "This is a test I would like to print, it should fill out all the page i have, I will be hopeful i can implement a display for messages that are way bigger then the cappacity of the screen (meaning it is longer then the amount of characters the screen can print out). This message is strictly for development / testing I have no other intentions of the usage of this message / char arrar / char pointer...";

// ============ PURE MENU FUNCTIONS ============
Temperature* temperature = new Temperature("TEMP", &carrier.Env);
Humidity* humidity = new Humidity("HUMIDITY", &carrier.Env);
KeyboardModule* keyboardModule = new KeyboardModule("KEYBOARD", testMessage);
DisplayMessage* displayMessage = new DisplayMessage("TEXTVIEW", testMessagePrint);

// ============ TESTING MENU ============
MenuControl* testMenu = new MenuControl(temperature, humidity, keyboardModule, displayMessage, &displayControl, &buttonControl, &carrier);

// ============ MENUS ============
MenuControl* enviornmentMenu = new MenuControl(temperature, humidity, temperature, temperature, &displayControl, &buttonControl, &carrier);
MenuControl* settingstMenu = new MenuControl(keyboardModule, displayMessage, temperature, temperature, &displayControl, &buttonControl, &carrier);

MenuControl mainMenu(enviornmentMenu, settingstMenu, testMenu, displayMessage, &displayControl, &buttonControl, &carrier);





void setup() {
  Serial.begin(9600);
  CARRIER_CASE = true;
  enviornmentMenu->setName("ENV_MENU");
  settingstMenu->setName("Settings");
  testMenu->setName("TEST_MENU");
  mainMenu.setName("Main_Menu");
  carrier.begin();
  //mainMenu.menuAction();

}

void loop() {
  //testMenu.menuAction();
  mainMenu.menuAction();

}
