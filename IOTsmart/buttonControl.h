#ifndef IOTSMART_BUTTONCONTROL_H
#define IOTSMART_BUTTONCONTROL_H

class ButtonControl{
  MKRIoTCarrierQtouch* button;
  public:
    ButtonControl(MKRIoTCarrierQtouch * button){
        this->button = button;
    }

    int checkButton(int state=0){
      if(state == 0){
        button->update();
        if(button->onTouchDown(TOUCH0)){
          return 0;
        }
        else if(button->onTouchDown(TOUCH1)){
          return 1;
        }
        else if(button->onTouchDown(TOUCH2)){
          return 2;
        }
        else if(button->onTouchDown(TOUCH3)){
          return 3;
        }
        else if(button->onTouchDown(TOUCH4)){
          return 4;
        }
      }
      else if(state == 1){
        button->update();
        if(button->getTouch(TOUCH0)){
          return 0;
        }
        else if(button->getTouch(TOUCH1)){
          return 1;
        }
        else if(button->getTouch(TOUCH2)){
          return 2;
        }
        else if(button->getTouch(TOUCH3)){
          return 3;
        }
        else if(button->getTouch(TOUCH4)){
          return 4;
        }
      }
      return -1;
    }
};


#endif //IOTSMART_BUTTONCONTROL_H
