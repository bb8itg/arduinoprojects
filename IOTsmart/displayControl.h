//
// Created by amibi on 2022. 03. 26..
//

#ifndef IOTSMART_DISPLAYCONTROL_H
#define IOTSMART_DISPLAYCONTROL_H

class DisplayControl{
    Adafruit_ST7789* display;
    public:
        DisplayControl(Adafruit_ST7789* display){
            this->display = display;
        }

        void drawMenu(String menu_1, String menu_2, String menu_3, String menu_4){
          display->fillScreen(ST77XX_BLACK);
          display->setTextColor(ST77XX_BLUE);
          display->setTextSize(1);
          display->drawLine(display->width()/2, 0, display->width()/2, display->width(), ST77XX_BLUE); //upper line
          display->drawLine(0, display->width()/2, display->width(),  display->width()/2, ST77XX_BLUE); // midle line

          display->setCursor(( display->width()/2 - display->width()/3 ), ( display->width()/2 - display->width()/4 ) ); // upper Left menu
          display->print(menu_1);
          display->setCursor(( display->width()/2 - display->width()/3 ), ( display->width()/2 + display->width()/4 ) ); // down Left menu
          display->print(menu_2);
          display->setCursor(( display->width()/2 + display->width()/6 ), ( display->width()/2 - display->width()/4 ) ); // upper Right menu
          display->print(menu_3);
          display->setCursor(( display->width()/2 + display->width()/6 ), ( display->width()/2 + display->width()/4 ) ); // down Right menu
          display->print(menu_4);

        }

        Adafruit_ST7789* getDisplay(){ return display;}
};

#endif //IOTSMART_DISPLAYCONTROL_H
