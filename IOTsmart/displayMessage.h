//
// Created by amibi on 2022. 03. 28..
//

#ifndef IOTSMART_DISPLAYMESSAGE_H
#define IOTSMART_DISPLAYMESSAGE_H

class DisplayMessage : public MenuOption{

    char* message;
    int counterJump = 13, rowPerScreen = 10;
    int rowJump = 16, colJump = 13, messageBufferIndex = 0;
    
    public:
      DisplayMessage(String nameOfOption, char* message) : MenuOption{nameOfOption}{
          this->message = message;
      }
      
      void processMenuFunction(ButtonControl* buttonControl, Adafruit_ST7789* display){
        int rowCount = 0;
        drawMenuOption(display, &rowCount);

        while(true){
          int buttonstate = buttonControl->checkButton();
          if(buttonstate == 3){ // UP
            if(rowCount > rowPerScreen*2-1){
              rowCount -= rowPerScreen*2;
              drawMenuOption(display, &rowCount);
            } else{
              rowCount = 0;
              drawMenuOption(display, &rowCount);
            }
        } else if(buttonstate == 1){ // DOWN
            //if(messageBufferIndex < bufferSize-1){
              drawMenuOption(display, &rowCount);
           // }
          }
          else if(buttonstate == 2){
            break;
          }
          Serial.println(buttonstate);
          delay(delayTime);
        }
      }

      void backgroundTask(){};
      void drawMenuOption(Adafruit_ST7789* display, int* rowCount){
        display->fillScreen(ST77XX_BLACK);        
        display->setTextColor(ST77XX_WHITE);
        display->setTextSize(2);
        display->setCursor(30,40);
        int counter = 0;
        int coulNum = 0, rowNum = 0;
        int rowCountLimit = *rowCount + rowPerScreen;
        for(int i = *rowCount * counterJump; message[i] != '\0' && *rowCount < rowCountLimit; i++){
          counter++;
          if(counter > counterJump){
            counter = 0;
            rowNum += rowJump;
            coulNum = 0;
            (*rowCount)++;
          } else{
            coulNum += colJump;
          }
          display->print(message[i]);
          display->setCursor(30 + coulNum,40 + rowNum);

        }
      };

};








#endif //IOTSMART_DISPLAYMESSAGE_H
