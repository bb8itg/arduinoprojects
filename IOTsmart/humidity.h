//
// Created by amibi on 2022. 03. 27..
//

#ifndef IOTSMART_HUMIDITY_H
#define IOTSMART_HUMIDITY_H

class Humidity : public MenuOption{
    HTS221Class* env;
public:
    Humidity(String nameOfOption, HTS221Class* env) : MenuOption{nameOfOption}{
        this->env = env;
    }
    virtual void processMenuFunction(ButtonControl* buttonControl, Adafruit_ST7789* display){
        double prevHumidity = env->readTemperature();
        drawMenuOption(display);
        delay(delayTime);
        while(buttonControl->checkButton() != 2){
            double humidity = env->readHumidity();
            if(humidity < prevHumidity - 1){
              prevHumidity = humidity;
              drawMenuOption(display);
            }
            if(humidity > prevHumidity + 1){
              prevHumidity = humidity;
              drawMenuOption(display);
            }
            delay(delayTime);
        }
    }


    void drawMenuOption(Adafruit_ST7789* display){
        display->fillScreen(ST77XX_BLACK);
        display->setTextSize(3);

        display->setCursor(display->width()/8, display->width()/3);
        display->print("Humidity");
        display->setCursor(display->width()/3, display->width()/2);
        display->print(env->readHumidity());
        delay(delayTime);
    }


    virtual void backgroundTask(){};
};

#endif //IOTSMART_HUMIDITY_H
