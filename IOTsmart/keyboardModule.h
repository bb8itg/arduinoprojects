//
// Created by amibi on 2022. 03. 27..
//

#ifndef IOTSMART_KEYBOARDMODULE_H
#define IOTSMART_KEYBOARDMODULE_H
#define keyboardSize 95
#define bufferSize 256

class KeyboardModule : public MenuOption{
    char keyboardCharSet[keyboardSize];
    char messageBuffer[bufferSize];
    char* dest;
    int counterJump = 15;
    int rowJump = 15, colJump = 13, messageBufferIndex = 0;

public:
    KeyboardModule(String nameOfOption, char* dest) : MenuOption{nameOfOption}{
        for(int i=32; i < 127; i++){
            keyboardCharSet[i - 32] = i;
        }
        this->dest = dest;

    }
    virtual void processMenuFunction(ButtonControl* buttonControl, Adafruit_ST7789* display){
      int selectedChar = 0, prevChar = 0, prevX = -1, prevY = -1;
      drawMenuOption(display);
      while(buttonControl->checkButton(1) != 2){
        if(buttonControl->checkButton(1) == 0){ //GO RIGHT
          if(selectedChar < keyboardSize-1){
            prevChar = selectedChar;
            selectedChar += 1;
            drawCursorJump(display, selectedChar, prevChar, &prevX, &prevY);
          } else{
            prevChar = keyboardSize-1;
            selectedChar = 0;
            drawCursorJump(display, selectedChar, prevChar, &prevX, &prevY);
          }
        } else if(buttonControl->checkButton(1) == 4){ //GO LEFT
          if(selectedChar > 0){
            prevChar = selectedChar;
            selectedChar -= 1;
            drawCursorJump(display, selectedChar, prevChar, &prevX, &prevY);
          } else{
            prevChar = 0;
            selectedChar = keyboardSize-1;
            drawCursorJump(display, selectedChar, prevChar, &prevX, &prevY);
          }
        } else if(buttonControl->checkButton(1) == 3){ // DEL
          if(messageBufferIndex > 0){
            messageBuffer[messageBufferIndex] = ' ';
            messageBufferIndex--;
            drawCursorMessage(display);

          }
        } else if(buttonControl->checkButton(1) == 1){ // TYPE
          if(messageBufferIndex < bufferSize-1){
            messageBuffer[messageBufferIndex] = keyboardCharSet[selectedChar];
            messageBufferIndex++;
            drawCursorMessage(display);
  
          }
        }       
        delay(delayTime*2);
      }
      delay(50);
      display->fillRect(display->width()/5, display->width()/5, 3*display->width()/5, 2*display->width()/7, ST77XX_WHITE);
      display->fillRect(display->width()/5+2, display->width()/5+2, 3*display->width()/5-5, 2*display->width()/7-4, ST77XX_BLACK);
      display->drawLine(display->width()/2 , display->width()/5, display->width()/2 , 2*display->width()/5+15, ST77XX_WHITE); //middle line
      
      display->setCursor(display->width()/5 + 10, display->width()/5 + 25);
      display->setTextColor(ST77XX_RED);
      display->print("DISC");
      display->setCursor(3*display->width()/5 - 15, display->width()/5 + 25);
      display->setTextColor(ST77XX_GREEN);
      display->print("SEND");
      display->setTextColor(ST77XX_BLUE);

      while(true){
        if(buttonControl->checkButton(1) == 3){ // DISC
          memset(messageBuffer,0,256);
          messageBufferIndex = 0;
          break;
        } else if(buttonControl->checkButton(1) == 1){ // SEND
          messageBufferIndex = 0;
          strcpy(dest, messageBuffer);
          memset(messageBuffer,0,256);
          break;
        }
      }
      delay(50);
      Serial.println(dest);
      delay(50);
    }

    
    void drawCursorMessage(Adafruit_ST7789* display){
      display->setTextColor(ST77XX_WHITE);
      int colNum = colJump;
      int rowNum = 0;
      int counter = 0;
      display->fillRect(0,0, display->width(), display->width()/8 + colNum*3+2, ST77XX_BLACK);
      for(int i=0; i < messageBufferIndex; i++){
        counter++;
        if(counter < 13){
          display->setCursor(display->width()/8 + colNum, display->width()/8 + rowNum);
          display->print(messageBuffer[i]);
          colNum += colJump;
        } else{
          colNum = colJump;
          counter = 0;
          rowNum += colJump;
          display->setCursor(display->width()/8 + colNum, display->width()/8  + rowNum);
          colNum += colJump;
          display->print(messageBuffer[i]);
        }
      }
      display->setTextColor(ST77XX_BLUE);
    }

    
    void drawCursorJump(Adafruit_ST7789* display,int selectedChar, int prevChar, int* prevX,int* prevY){
      int counter = 0;
      int rowNum = 0;
      int colNum = 0;
      for(int i=0; i < keyboardSize; i++){
        counter +=1;
        if(counter > counterJump){
          rowNum+=rowJump;
          colNum = 0;
          display->setCursor(display->width()/10, display->width()/3+rowNum);
          counter = 0;
        }
        if( i == selectedChar){
          if(*prevX != -1 && *prevY != -1 ){
            display->setTextColor(ST77XX_BLUE);
            display->setCursor(*prevX, *prevY);
            display->print(keyboardCharSet[prevChar]);
            
            display->setTextColor(ST77XX_WHITE);
            *prevX = display->width()/10 + colNum;
            *prevY = display->width()/3+rowNum;
            display->setCursor(*prevX, *prevY);
            display->print(keyboardCharSet[selectedChar]);
  
            display->setTextColor(ST77XX_BLUE);  
          } else{
            display->setTextColor(ST77XX_WHITE);
            *prevX = display->width()/10 + colNum;
            *prevY = display->width()/3+rowNum;
            display->setCursor(*prevX, *prevY);
            display->setTextColor(ST77XX_BLUE);
          }
        }
        colNum += colJump;
        }
    }

    void drawMenuOption(Adafruit_ST7789* display){
      display->fillScreen(ST77XX_BLACK);
      display->setTextColor(ST77XX_BLUE);
      display->drawLine(0, display->width()/3 - 5, display->width(), display->width()/3 - 5, ST77XX_BLUE); //upper line
      display->fillRect(0, display->width() - display->width()/4, display->width(), display->width() - display->width()/4, ST77XX_BLUE); //bottom line
      display->drawLine(display->width()/2, display->width() - display->width()/4, display->width()/2, display->width(), ST77XX_WHITE);
      display->setTextSize(2);
      display->setCursor(display->width()/10, display->width()/3);
      int counter = 0;
      int rowNum = 0;
      int colNum = 0;
      for(int i=0; i < keyboardSize; i++){
        counter +=1;
        if(counter > counterJump){
          rowNum+=rowJump;
          colNum = 0;
          display->setCursor(display->width()/10, display->width()/3+rowNum);
          counter = 0;
      }
      display->setCursor(display->width()/10 + colNum, display->width()/3+rowNum);
      display->print(keyboardCharSet[i]);
      colNum += colJump;
      }
      display->setTextColor(ST77XX_WHITE);
      display->setCursor(display->width()/2 - display->width()/4, display->width() - display->width()/5);
      display->print("DEL");
      display->setCursor(2*display->width()/3 - 20, display->width() - display->width()/5);
      display->print("TYPE");
      display->setTextColor(ST77XX_BLUE);
      //display->setCursor(display->width()/10 + colNum, display->width()/3+rowNum);
      
    }


    virtual void backgroundTask(){};
};

#endif //IOTSMART_KEYBOARDMODULE_H
