//
// Created by amibi on 2022. 03. 26..
//

#ifndef IOTSMART_MENUCONTROLL_H
#define IOTSMART_MENUCONTROLL_H

#include "menuOption.h"
#include "buttonControl.h"

class MenuControl : public MenuOption {
    private:
        MenuOption* menuOptions[4];
        ButtonControl* buttonControl;
        DisplayControl* displayControl;
        MKRIoTCarrier* carrier;
        
    public:
        MenuControl(MenuOption* menuOption, DisplayControl* displayControl, ButtonControl* buttonControl, MKRIoTCarrier* carrier){
            this->buttonControl = buttonControl;
            this->carrier = carrier;
            menuOptions[0] = menuOption;
            menuOptions[1] = new MenuOption();
            menuOptions[2] = new MenuOption();
            menuOptions[3] = new MenuOption();
            this->displayControl = displayControl;

        }

        MenuControl(MenuOption* menuOption_1, MenuOption* menuOption_2 , DisplayControl* displayControl, ButtonControl* buttonControl, MKRIoTCarrier* carrier){
            this->buttonControl = buttonControl;
            this->carrier = carrier;
            menuOptions[0] = menuOption_1;
            menuOptions[1] = menuOption_2;
            menuOptions[2] = new MenuOption();
            menuOptions[3] = new MenuOption();
            this->displayControl = displayControl;
        }
        
        MenuControl(MenuOption* menuOption_1, MenuOption* menuOption_2, MenuOption* menuOption_3, DisplayControl* displayControl, ButtonControl* buttonControl, MKRIoTCarrier* carrier){
            this->buttonControl = buttonControl;
            this->carrier = carrier;
            menuOptions[0] = menuOption_1;
            menuOptions[1] = menuOption_2;
            menuOptions[2] = menuOption_3;
            menuOptions[3] = new MenuOption();
            this->displayControl = displayControl;

        }

        MenuControl(MenuOption* menuOption_1, MenuOption* menuOption_2, MenuOption* menuOption_3, MenuOption* menuOption_4, DisplayControl* displayControl, ButtonControl* buttonControl, MKRIoTCarrier* carrier){
            this->buttonControl = buttonControl;
            this->carrier = carrier;
            menuOptions[0] = menuOption_1;
            menuOptions[1] = menuOption_2;
            menuOptions[2] = menuOption_3;
            menuOptions[3] = menuOption_4;
            this->displayControl = displayControl;

        }
        
        ~MenuControl(){
          for(int i=0; i < 4;i++){
            delete menuOptions[i];
          }
        }

        
        void menuAction(){
          delay(delayTime*10);
          bool inMenu = true;
          while(inMenu){
           int buttonstate = buttonControl->checkButton();
           switch (buttonstate){
            case 0: // menuOptions[2]
              menuOptions[2]->processMenuFunction(buttonControl, displayControl->getDisplay());
              menuOptions[2]->menuAction();
              displayControl->drawMenu(menuOptions[0]->getName(), menuOptions[1]->getName(), menuOptions[2]->getName(), menuOptions[3]->getName());
              break;
            case 1: // menuOptions[3]
              menuOptions[3]->processMenuFunction(buttonControl, displayControl->getDisplay());
              menuOptions[3]->menuAction();
              displayControl->drawMenu(menuOptions[0]->getName(), menuOptions[1]->getName(), menuOptions[2]->getName(), menuOptions[3]->getName());
              break;
            case 2: //THIS IS SKIP BUTTON
              inMenu = false;
              displayControl->drawMenu(menuOptions[0]->getName(), menuOptions[1]->getName(), menuOptions[2]->getName(), menuOptions[3]->getName());
              break;
            case 3: // menuOptions[1]
              menuOptions[1]->processMenuFunction(buttonControl, displayControl->getDisplay());
              menuOptions[1]->menuAction();
              displayControl->drawMenu(menuOptions[0]->getName(), menuOptions[1]->getName(), menuOptions[2]->getName(), menuOptions[3]->getName());
              break;
            case 4: // menuOptions[0]
              menuOptions[0]->processMenuFunction(buttonControl, displayControl->getDisplay());
              menuOptions[0]->menuAction();
              displayControl->drawMenu(menuOptions[0]->getName(), menuOptions[1]->getName(), menuOptions[2]->getName(), menuOptions[3]->getName());
              break;
           }
           
           delay(delayTime);
           }
        }
        
        virtual void processMenuFunction(ButtonControl* buttonControl, Adafruit_ST7789* display){
        }

        virtual void backgroundTask(){};
};

#endif //IOTSMART_MENUCONTROLL_H
