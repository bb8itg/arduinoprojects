//
// Created by amibi on 2022. 03. 26..
//

#ifndef IOTSMART_MENUOPTION_H
#define IOTSMART_MENUOPTION_H

#include <string.h>

class MenuOption{
    private:
        String nameOfOption = "TEST";
    protected:
        int delayTime = 50;

    public:
        MenuOption(){}
        MenuOption(String nameOfOption){
            this->nameOfOption = nameOfOption;
        }

        virtual void menuAction(){}
        void setName(String nameOfOption){  this->nameOfOption = nameOfOption;};
        virtual void processMenuFunction(ButtonControl* buttonControl, Adafruit_ST7789* display){};
        virtual void backgroundTask(){};
        virtual void drawMenuOption(Adafruit_ST7789* display){};
        String getName(){return nameOfOption;}
};

#endif //IOTSMART_MENUOPTION_H
