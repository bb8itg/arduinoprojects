//
// Created by amibi on 2022. 03. 26..
//

#ifndef IOTSMART_TEMPERATURE_H
#define IOTSMART_TEMPERATURE_H

class Temperature : public MenuOption{
    HTS221Class* env;
    public:
        Temperature(String nameOfOption, HTS221Class* env) : MenuOption{nameOfOption}{
          this->env = env;
        }
        virtual void processMenuFunction(ButtonControl* buttonControl, Adafruit_ST7789* display){
          double prevTemp = env->readTemperature();
          drawMenuOption(display);
          delay(delayTime);
          while(buttonControl->checkButton() != 2){
            double temp = env->readTemperature();
            if(temp < prevTemp - 1){
              prevTemp = temp;
              drawMenuOption(display);
            }
             if(temp > prevTemp + 1){
              prevTemp = temp;
              drawMenuOption(display);
            }
            delay(delayTime);
          }
        }

        
        void drawMenuOption(Adafruit_ST7789* display){
            display->fillScreen(ST77XX_BLACK);
            display->setTextSize(3);

            display->setCursor(display->width()/8, display->width()/3);
            display->print("Temperature");
            display->setCursor(display->width()/3, display->width()/2);
            display->print(env->readTemperature() - 4);
            delay(delayTime);
        }

        
        virtual void backgroundTask(){};
};


#endif //IOTSMART_TEMPERATURE_H
